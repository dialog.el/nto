﻿namespace NTO
{
    partial class DHotels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DHotels));
            dgvDHotels = new DataGridView();
            Hotel = new DataGridViewTextBoxColumn();
            Category = new DataGridViewTextBoxColumn();
            btnDHotels = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvDHotels).BeginInit();
            SuspendLayout();
            // 
            // dgvDHotels
            // 
            dgvDHotels.AllowUserToAddRows = false;
            dgvDHotels.AllowUserToDeleteRows = false;
            dgvDHotels.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvDHotels.Columns.AddRange(new DataGridViewColumn[] { Hotel, Category });
            dgvDHotels.Location = new Point(24, 41);
            dgvDHotels.Name = "dgvDHotels";
            dgvDHotels.ReadOnly = true;
            dgvDHotels.RowHeadersVisible = false;
            dgvDHotels.RowTemplate.Height = 25;
            dgvDHotels.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvDHotels.Size = new Size(721, 345);
            dgvDHotels.TabIndex = 0;
            // 
            // Hotel
            // 
            Hotel.DataPropertyName = "Name";
            Hotel.HeaderText = "Отель";
            Hotel.Name = "Hotel";
            Hotel.ReadOnly = true;
            Hotel.Width = 500;
            // 
            // Category
            // 
            Category.DataPropertyName = "CategoryName";
            Category.HeaderText = "Категория";
            Category.Name = "Category";
            Category.ReadOnly = true;
            Category.Width = 150;
            // 
            // btnDHotels
            // 
            btnDHotels.BackColor = SystemColors.ActiveCaption;
            btnDHotels.Location = new Point(603, 392);
            btnDHotels.Name = "btnDHotels";
            btnDHotels.Size = new Size(142, 37);
            btnDHotels.TabIndex = 1;
            btnDHotels.Text = "Изменить";
            btnDHotels.UseVisualStyleBackColor = false;
            btnDHotels.Click += btnDHotels_Click;
            // 
            // DHotels
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            BackgroundImageLayout = ImageLayout.Stretch;
            ClientSize = new Size(800, 450);
            Controls.Add(btnDHotels);
            Controls.Add(dgvDHotels);
            Name = "DHotels";
            Text = "Направление отелей";
            Load += DHotels_Load;
            ((System.ComponentModel.ISupportInitialize)dgvDHotels).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dgvDHotels;
        private Button btnDHotels;
        private DataGridViewTextBoxColumn Hotel;
        private DataGridViewTextBoxColumn Category;
    }
}