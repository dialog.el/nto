﻿using Microsoft.EntityFrameworkCore;
using NTO.database;
using NTO.database.models;

namespace NTO
{
    public partial class Enter : Form
    {
        public Enter()
        {
            InitializeComponent();
        }

        private void Enter_Load(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                cbEnter.DataSource = context.GetBookingsForEnterAction();
            }
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            ModelBooking model = cbEnter.SelectedItem as ModelBooking;
            using (Context context = new())
            {
                var room = context.Rooms.Where(t => t.Id == model.RoomId).Single();
                room.StatusId = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf14"); // Чистый и занятый

                context.Entry(model).State = EntityState.Modified;

                model.isCompleteEnter = true;
                context.SaveChanges();
            }

            MessageBox.Show($"Бронь снята, статус '{model.Room.Number}' комнаты теперь 'Чистый и занятый'");
        }

        private void btnEnter2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Форма бронирования, которой нет :)");
        }
    }
}
