﻿namespace NTO
{
    partial class Categories
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Categories));
            dgvCategories = new DataGridView();
            btnCategories = new Button();
            button1 = new Button();
            ((System.ComponentModel.ISupportInitialize)dgvCategories).BeginInit();
            SuspendLayout();
            // 
            // dgvCategories
            // 
            dgvCategories.AllowUserToAddRows = false;
            dgvCategories.AllowUserToDeleteRows = false;
            dgvCategories.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvCategories.Location = new Point(25, 27);
            dgvCategories.Name = "dgvCategories";
            dgvCategories.ReadOnly = true;
            dgvCategories.RowTemplate.Height = 25;
            dgvCategories.SelectionMode = DataGridViewSelectionMode.CellSelect;
            dgvCategories.Size = new Size(728, 351);
            dgvCategories.TabIndex = 0;
            // 
            // btnCategories
            // 
            btnCategories.BackColor = SystemColors.ActiveCaption;
            btnCategories.Location = new Point(581, 398);
            btnCategories.Name = "btnCategories";
            btnCategories.Size = new Size(172, 40);
            btnCategories.TabIndex = 1;
            btnCategories.Text = "Изменить";
            btnCategories.UseVisualStyleBackColor = false;
            btnCategories.Click += btnCategories_Click;
            // 
            // button1
            // 
            button1.BackColor = SystemColors.ActiveCaption;
            button1.Location = new Point(403, 398);
            button1.Name = "button1";
            button1.Size = new Size(172, 40);
            button1.TabIndex = 2;
            button1.Text = "К отчету";
            button1.UseVisualStyleBackColor = false;
            button1.Click += button1_Click;
            // 
            // Categories
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            BackgroundImageLayout = ImageLayout.Stretch;
            ClientSize = new Size(800, 450);
            Controls.Add(button1);
            Controls.Add(btnCategories);
            Controls.Add(dgvCategories);
            Name = "Categories";
            Text = "Категории";
            Load += Categories_Load_1;
            ((System.ComponentModel.ISupportInitialize)dgvCategories).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dgvCategories;
        private Button btnCategories;
        private Button button1;
    }
}