﻿namespace NTO
{
    partial class FornNav
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FornNav));
            btn1 = new Button();
            buttonGuest = new Button();
            buttonClients = new Button();
            button1 = new Button();
            button2 = new Button();
            button3 = new Button();
            button4 = new Button();
            btnDHotels = new Button();
            btnDied = new Button();
            button5 = new Button();
            SuspendLayout();
            // 
            // btn1
            // 
            btn1.BackColor = SystemColors.ActiveCaption;
            btn1.Location = new Point(12, 12);
            btn1.Name = "btn1";
            btn1.Size = new Size(352, 51);
            btn1.TabIndex = 0;
            btn1.Text = "Отели";
            btn1.UseVisualStyleBackColor = false;
            btn1.Click += btn1_Click;
            // 
            // buttonGuest
            // 
            buttonGuest.BackColor = SystemColors.ActiveCaption;
            buttonGuest.Location = new Point(12, 126);
            buttonGuest.Name = "buttonGuest";
            buttonGuest.Size = new Size(352, 51);
            buttonGuest.TabIndex = 1;
            buttonGuest.Text = "Гости";
            buttonGuest.UseVisualStyleBackColor = false;
            buttonGuest.Click += buttonGuest_Click;
            // 
            // buttonClients
            // 
            buttonClients.BackColor = SystemColors.ActiveCaption;
            buttonClients.Location = new Point(12, 183);
            buttonClients.Name = "buttonClients";
            buttonClients.Size = new Size(352, 51);
            buttonClients.TabIndex = 2;
            buttonClients.Text = "Клиенты";
            buttonClients.UseVisualStyleBackColor = false;
            buttonClients.Click += buttonClients_Click;
            // 
            // button1
            // 
            button1.BackColor = SystemColors.ActiveCaption;
            button1.Location = new Point(12, 348);
            button1.Name = "button1";
            button1.Size = new Size(352, 51);
            button1.TabIndex = 3;
            button1.Text = "Заезд";
            button1.UseVisualStyleBackColor = false;
            button1.Click += buttonEnter;
            // 
            // button2
            // 
            button2.BackColor = SystemColors.ActiveCaption;
            button2.Location = new Point(12, 406);
            button2.Name = "button2";
            button2.Size = new Size(352, 51);
            button2.TabIndex = 4;
            button2.Text = "Выезд";
            button2.UseVisualStyleBackColor = false;
            button2.Click += buttonExit;
            // 
            // button3
            // 
            button3.BackColor = SystemColors.ActiveCaption;
            button3.Location = new Point(12, 69);
            button3.Name = "button3";
            button3.Size = new Size(352, 51);
            button3.TabIndex = 5;
            button3.Text = "Журнал бронирования";
            button3.UseVisualStyleBackColor = false;
            button3.Click += button3_Click;
            // 
            // button4
            // 
            button4.BackColor = SystemColors.ActiveCaption;
            button4.Location = new Point(12, 240);
            button4.Name = "button4";
            button4.Size = new Size(352, 51);
            button4.TabIndex = 6;
            button4.Text = "Ценообразование категорий";
            button4.UseVisualStyleBackColor = false;
            button4.Click += button4_Click;
            // 
            // btnDHotels
            // 
            btnDHotels.BackColor = SystemColors.ActiveCaption;
            btnDHotels.Location = new Point(12, 496);
            btnDHotels.Name = "btnDHotels";
            btnDHotels.Size = new Size(352, 52);
            btnDHotels.TabIndex = 7;
            btnDHotels.Text = "Направление отелей";
            btnDHotels.UseVisualStyleBackColor = false;
            btnDHotels.Click += btnDHotels_Click;
            // 
            // btnDied
            // 
            btnDied.BackColor = SystemColors.ActiveCaption;
            btnDied.Location = new Point(12, 554);
            btnDied.Name = "btnDied";
            btnDied.Size = new Size(352, 51);
            btnDied.TabIndex = 8;
            btnDied.Text = "\"Мертвый\" сезон";
            btnDied.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            button5.BackColor = SystemColors.ActiveCaption;
            button5.Location = new Point(12, 637);
            button5.Name = "button5";
            button5.Size = new Size(352, 51);
            button5.TabIndex = 9;
            button5.Text = "Анализ эффективности продаж";
            button5.UseVisualStyleBackColor = false;
            button5.Click += button5_Click;
            // 
            // FornNav
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            BackgroundImageLayout = ImageLayout.Stretch;
            ClientSize = new Size(376, 713);
            Controls.Add(button5);
            Controls.Add(btnDied);
            Controls.Add(btnDHotels);
            Controls.Add(button4);
            Controls.Add(button3);
            Controls.Add(button2);
            Controls.Add(button1);
            Controls.Add(buttonClients);
            Controls.Add(buttonGuest);
            Controls.Add(btn1);
            Name = "FornNav";
            Text = "Главное меню";
            ResumeLayout(false);
        }

        #endregion

        private Button btn1;
        private Button buttonGuest;
        private Button buttonClients;
        private Button button1;
        private Button button2;
        private Button button3;
        private Button button4;
        private Button btnDHotels;
        private Button btnDied;
        private Button button5;
    }
}