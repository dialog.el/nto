﻿using NTO.database;
using NTO.database.models;

namespace NTO
{
    public partial class Room : Form
    {
        Guid hotelId;
        List<ModelRoom> rooms;

        public Room(Guid hotelId)
        {
            InitializeComponent();
            this.hotelId = hotelId;
        }

        private void Room_Load(object sender, EventArgs e)
        {
            dgvRoom.AutoGenerateColumns = false;
            using (Context context = new())
            {
                rooms = context.GetRoomsHotel(hotelId);
                dgvRoom.DataSource = rooms;
            }
        }

        private void buttonRoom_Click(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                var form = new EditRoom(context.Hotels.Where(t => t.Id == hotelId).Single().Id);
                form.Closed += Refresh;
                form.Show();
            }

        }

        public void Refresh(object sender, EventArgs e)
        {
            dgvRoom.AutoGenerateColumns = false;
            using (Context context = new())
            {
                rooms = context.GetRoomsHotel(hotelId);
                dgvRoom.DataSource = rooms;
            }
        }
    }
}
