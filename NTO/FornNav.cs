using NTO.database;

namespace NTO
{
    public partial class FornNav : Form
    {
        public FornNav()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void buttonGuest_Click(object sender, EventArgs e)
        {
            Guests form = new Guests();
            form.Show();
        }

        private void buttonClients_Click(object sender, EventArgs e)
        {
            Clients clients = new Clients();
            clients.Show();
        }

        private void buttonEnter(object sender, EventArgs e)
        {
            // �����
            new Enter().Show();
        }

        private void buttonExit(object sender, EventArgs e)
        {
            new Exit().Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new Booking().Show();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            Hotel form = new Hotel();
            form.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Categories form = new Categories();
            form.Show();
        }

        private void btnDHotels_Click(object sender, EventArgs e)
        {
            DHotels form = new DHotels();
            form.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            NormalRep form = new NormalRep();
            form.Show();
        }
    }
}