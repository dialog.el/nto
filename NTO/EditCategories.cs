﻿using Microsoft.EntityFrameworkCore;
using NTO.database;
using NTO.database.models;

namespace NTO
{
    internal partial class EditCategories : Form
    {
        ModelCategory category;
        ModelHotel hotel;
        bool isFirstStart = true;

        public EditCategories(ModelCategory category, ModelHotel hotel)
        {
            this.category = category;
            this.hotel = hotel;
            InitializeComponent();
        }

        private void btnEditCategories_Click(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                category.Price = (int)numericUpDownCost.Value;
                context.Entry(category).State = EntityState.Modified;
                context.Update(category);
                context.SaveChanges();
            }
            MessageBox.Show("Успешно");
            Close();
        }

        private void EditCategories_Load(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                var data = context.GetCategories().Where(t => t.HotelId == hotel.Id).ToList();
                comboBoxCategory.DataSource = data;
                comboBoxCategory.SelectedIndex = data.Select(t => t.Name).ToList().IndexOf(category.Name);
                comboBoxHotel.DataSource = context.GetHotels();
                numericUpDownCost.Value = category.Price;
            }

            isFirstStart = false;
        }

        private void comboBoxCategory_TextChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void comboBoxHotel_SelectedIndexChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        public void Refresh()
        {
            if (!isFirstStart)
            {
                using (Context context = new())
                {
                    comboBoxCategory.DataSource = context.GetCategories().Where(t => t.HotelId == hotel.Id).ToList();
                    numericUpDownCost.Value = (comboBoxCategory.SelectedItem as ModelCategory).Price;
                }
            }
        }
    }
}
