﻿using Microsoft.Reporting.WinForms;
using NTO.database;
using NTO.database.models;
using System.Data;

namespace NTO
{
    public partial class NormalRep : Form
    {
        public NormalRep()
        {
            InitializeComponent();
        }

        private void btnNRep_Click(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                DataTable dtM = new DataTable();
                var hotel = comboBoxHotels.SelectedItem as ModelHotel;
                reportViewer1.LocalReport.ReportEmbeddedResource = "NTO.NormalRep.rdlc";
                ReportDataSource rdsM = new ReportDataSource("DataSet1", dtM);
                reportViewer1.LocalReport.DataSources.Add(rdsM);
                context.FillParamenterReportViewer(reportViewer1, dateTimePickerEnd.Value, hotel.Name);
                reportViewer1.RefreshReport();
            }
        }

        private void NormalRep_Load(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                var hotels = context.GetHotels();
                comboBoxHotels.DataSource = hotels;
                dateTimePickerEnd.Value = DateTime.Today;
            }
        }

        private void panelNorRep_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
