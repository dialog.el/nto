﻿namespace NTO
{
    partial class DiedSeason
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiedSeason));
            dgvDiedSeason = new DataGridView();
            btnDiedSeason = new Button();
            Start = new DataGridViewTextBoxColumn();
            End = new DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)dgvDiedSeason).BeginInit();
            SuspendLayout();
            // 
            // dgvDiedSeason
            // 
            dgvDiedSeason.AllowUserToAddRows = false;
            dgvDiedSeason.AllowUserToDeleteRows = false;
            dgvDiedSeason.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvDiedSeason.Columns.AddRange(new DataGridViewColumn[] { Start, End });
            dgvDiedSeason.Location = new Point(23, 25);
            dgvDiedSeason.Name = "dgvDiedSeason";
            dgvDiedSeason.ReadOnly = true;
            dgvDiedSeason.RowTemplate.Height = 25;
            dgvDiedSeason.Size = new Size(744, 357);
            dgvDiedSeason.TabIndex = 0;
            // 
            // btnDiedSeason
            // 
            btnDiedSeason.Location = new Point(613, 397);
            btnDiedSeason.Name = "btnDiedSeason";
            btnDiedSeason.Size = new Size(154, 41);
            btnDiedSeason.TabIndex = 1;
            btnDiedSeason.Text = " Изменить";
            btnDiedSeason.UseVisualStyleBackColor = true;
            // 
            // Start
            // 
            Start.HeaderText = "Начало";
            Start.Name = "Start";
            Start.ReadOnly = true;
            // 
            // End
            // 
            End.HeaderText = "Конец";
            End.Name = "End";
            End.ReadOnly = true;
            // 
            // DiedSeason
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            BackgroundImageLayout = ImageLayout.Stretch;
            ClientSize = new Size(800, 450);
            Controls.Add(btnDiedSeason);
            Controls.Add(dgvDiedSeason);
            Name = "DiedSeason";
            Text = "\"Мёртвый\" сезон";
            ((System.ComponentModel.ISupportInitialize)dgvDiedSeason).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dgvDiedSeason;
        private DataGridViewTextBoxColumn Start;
        private DataGridViewTextBoxColumn End;
        private Button btnDiedSeason;
    }
}