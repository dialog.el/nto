﻿using NTO.database;
using NTO.database.models;

namespace NTO
{
    public partial class EditRoom : Form
    {
        Guid? HotelId;
        public EditRoom(Guid? HotelId = null)
        {
            this.HotelId = HotelId;
            InitializeComponent();
        }

        private void EditRoom_Load(object sender, EventArgs e)
        {
            countHumans.Minimum = 1;
            using (Context context = new())
            {
                var hotels = context.GetHotels();
                cbEdStatus.DataSource = context.Statuses.ToList();
                cbEdCategory.DataSource = context.GetCategories();
                cbHotel.DataSource = hotels;
                if (HotelId != null)
                {
                    cbHotel.SelectedIndex = hotels.Select(t => t.Id).ToList().IndexOf((Guid)HotelId);
                }
            }
            countHumans.Maximum = (cbEdCategory.SelectedItem as ModelCategory).MaxCountHumans;
        }

        private void btnEdRoom_Click(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                ModelRoom model = new ModelRoom();
                model.StatusId = ((ModelStatus)cbEdStatus.SelectedValue).Id;
                model.CategoryId = ((ModelCategory)cbEdCategory.SelectedValue).Id;
                model.HotelId = ((ModelHotel)cbHotel.SelectedValue).Id;
                model.Number = tbEdRoom.Text;
                model.CountHumans = (int)countHumans.Value;
                context.Add(model);
                context.SaveChanges();
                MessageBox.Show("Успешно");
                Close();
            }

        }

        private void cbEdCategory_ValueMemberChanged(object sender, EventArgs e)
        {

        }

        private void countHumans_ValueChanged(object sender, EventArgs e)
        {

        }

        private void cbEdCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            countHumans.Maximum = (cbEdCategory.SelectedItem as ModelCategory).MaxCountHumans;
        }
    }
}
