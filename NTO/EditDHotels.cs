﻿using Microsoft.EntityFrameworkCore;
using NTO.database;
using NTO.database.models;

namespace NTO
{
    public partial class EditDHotels : Form
    {

        public ModelHotel hotel;
        public List<ModelHotel> hotels;
        public List<ModelCategoryHotel> categories;

        public EditDHotels(ModelHotel hotel)
        {
            this.hotel = hotel;
            InitializeComponent();
        }

        private void btnEdDHotels_Click(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                hotel = cbEdDHotels.SelectedItem as ModelHotel;
                context.Entry(hotel).State = EntityState.Modified;
                hotel.CategoryHotel = cbEdDHotels2.SelectedItem as ModelCategoryHotel;
                context.Update(hotel);
                context.SaveChanges();
                MessageBox.Show("Успешно");
                Close();
            }
        }

        private void EditDHotels_Load(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                hotels = context.GetHotels();
                cbEdDHotels.DataSource = hotels;
                cbEdDHotels.SelectedIndex = hotels.Select(t => t.Name).ToList().IndexOf(hotel.Name);

                categories = context.CategoriesHotel.ToList();
                cbEdDHotels2.DataSource = categories;
                cbEdDHotels2.SelectedIndex = categories.Select(t => t.Name).ToList().IndexOf(hotel.CategoryName);
            }
        }
    }
}
