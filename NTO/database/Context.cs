﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Reporting.WinForms;
using NTO.database.models;
using System.Data;
using System.Diagnostics.Contracts;
using System.Xml.Linq;

namespace NTO.database
{
    internal class Context: DbContext
    {
        public static string PathDirectoryDB = "C:\\NTO\\";
        public static string NameFileDB = "finalnto.db";
        public static string ConnectionString = $"Data Source={PathDirectoryDB}\\{NameFileDB}";

        public Context() => Database.EnsureCreated();

        public DbSet<ModelHotel> Hotels => Set<ModelHotel>();
        public DbSet<ModelRegion> Regions => Set<ModelRegion>();
        public DbSet<ModelCategory> Categories => Set<ModelCategory>();
        public DbSet<ModelRoom> Rooms => Set<ModelRoom>();
        public DbSet<ModelGuest> Guests => Set<ModelGuest>();
        public DbSet<ModelClient> Clients => Set<ModelClient>();
        public DbSet<ModelBooking> Bookings => Set<ModelBooking>();
        public DbSet<ModelStatus> Statuses => Set<ModelStatus>();
        public DbSet<ModelAction> Actions => Set<ModelAction>();
        public DbSet<ListGuests> ListGuests => Set<ListGuests>();
        public DbSet<ModelCategoryHotel> CategoriesHotel => Set<ModelCategoryHotel>();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            Directory.CreateDirectory(PathDirectoryDB);
            optionsBuilder.UseSqlite(ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ModelRegion>().HasData(
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f5"), Name = "Москва" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f9"), Name = "Нижний Новгород" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f3"), Name = "Санкт-Петербург" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f0"), Name = "Киров" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f4"), Name = "Екатеринбург" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f1"), Name = "Ижевск" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f2"), Name = "Пенза" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f6"), Name = "Сыктывкар" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f7"), Name = "Калининград" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f8"), Name = "Павловский Посад" }
            ) ;
            modelBuilder.Entity<ModelHotel>().HasData(
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"), Name = "Весенние ласточки", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f5"), CategoryHotelId = Guid.Parse("00ae514e-2f8d-4b10-a939-904e276b7452") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f281"), Name = "Капель", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f9"), CategoryHotelId = Guid.Parse("6470ed37-b8f0-40e9-bbf6-fc6d5f5c0ff9") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f282"), Name = "Здоровый отдых", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f3"), CategoryHotelId = Guid.Parse("74c14e57-2bd2-4cc8-b829-97d742890dd6") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f283"), Name = "Переправа", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f0"), CategoryHotelId = Guid.Parse("00ae514e-2f8d-4b10-a939-904e276b7452") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f284"), Name = "Лесной Бор", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f4"), CategoryHotelId = Guid.Parse("74c14e57-2bd2-4cc8-b829-97d742890dd6") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f285"), Name = "Неограниченные возможности", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f1"), CategoryHotelId = Guid.Parse("00ae514e-2f8d-4b10-a939-904e276b7452") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f286"), Name = "Чистый лист", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f2"), CategoryHotelId = Guid.Parse("6470ed37-b8f0-40e9-bbf6-fc6d5f5c0ff9") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f288"), Name = "Солнечный день", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f6"), CategoryHotelId = Guid.Parse("6470ed37-b8f0-40e9-bbf6-fc6d5f5c0ff9") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f289"), Name = "Шоколад", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f7"), CategoryHotelId = Guid.Parse("74c14e57-2bd2-4cc8-b829-97d742890dd6") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f280"), Name = "Яблочный спас", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f8"), CategoryHotelId = Guid.Parse("6470ed37-b8f0-40e9-bbf6-fc6d5f5c0ff9") }
            );

            modelBuilder.Entity<ModelStatus>().HasData(
                new ModelStatus { Id = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf13"), Name = "Чистый и свободный" },
                new ModelStatus { Id = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf14"), Name = "Чистый и занят" },
                new ModelStatus { Id = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf15"), Name = "Грязный и свободный" },
                new ModelStatus { Id = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf16"), Name = "Грязный и занят" }
            );

            modelBuilder.Entity<ModelCategory>().HasData(
               new ModelCategory { Id = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91419"), Name = "Стандарт", MaxCountHumans = 2, Price=1000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287") },
               new ModelCategory { Id = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91420"), Name = "Люкс", MaxCountHumans = 2, Price = 2000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287") },
               new ModelCategory { Id = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91421"), Name = "Апартамент", MaxCountHumans = 4, Price = 3000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287") },

               new ModelCategory { Id = Guid.Parse("348d9c0b-70ef-41d2-a604-5de91b18e681"), Name = "Стандарт", MaxCountHumans = 2, Price = 1000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f281") },
               new ModelCategory { Id = Guid.Parse("d059c9dd-6c19-477a-bc22-257a7456f1d8"), Name = "Люкс", MaxCountHumans = 2, Price = 2000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f281") },
               new ModelCategory { Id = Guid.Parse("c6878c29-11d7-4768-bc1a-8740c58dd39e"), Name = "Апартамент", MaxCountHumans = 4, Price = 3000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f281") },

               new ModelCategory { Id = Guid.Parse("399a1f3c-4b32-4c31-8f0f-a23d10234113"), Name = "Стандарт", MaxCountHumans = 2, Price = 1000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f282") },
               new ModelCategory { Id = Guid.Parse("3a42e6c4-b2c9-4cac-85cd-c8293008fc86"), Name = "Люкс", MaxCountHumans = 2, Price = 2000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f282") },
               new ModelCategory { Id = Guid.Parse("2ff7080f-aded-4893-9804-9912cd704a96"), Name = "Апартамент", MaxCountHumans = 4, Price = 3000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f282") },

               new ModelCategory { Id = Guid.Parse("55531a6f-0689-4494-8fac-49ff5f2379ac"), Name = "Стандарт", MaxCountHumans = 2, Price = 1000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f283") },
               new ModelCategory { Id = Guid.Parse("6576f332-f63f-4f25-9a24-55d51b42d63c"), Name = "Люкс", MaxCountHumans = 2, Price = 2000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f283") },
               new ModelCategory { Id = Guid.Parse("2c8f896c-71d6-4a33-b2fb-223aaed0cd2d"), Name = "Апартамент", MaxCountHumans = 4, Price = 3000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f283") },

               new ModelCategory { Id = Guid.Parse("bea41485-857a-4abf-9029-fdb73591bd8e"), Name = "Стандарт", MaxCountHumans = 2, Price = 1000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f284") },
               new ModelCategory { Id = Guid.Parse("b9eb119f-15a7-4004-99aa-161067ef79fe"), Name = "Люкс", MaxCountHumans = 2, Price = 2000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f284") },
               new ModelCategory { Id = Guid.Parse("d7dde157-4fed-479a-b694-e7ca805a2d0b"), Name = "Апартамент", MaxCountHumans = 4, Price = 3000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f284") },

               new ModelCategory { Id = Guid.Parse("6eef0d93-49a7-456a-af55-ffff29cea71f"), Name = "Стандарт", MaxCountHumans = 2, Price = 1000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f285") },
               new ModelCategory { Id = Guid.Parse("a0ce6c1e-c997-4bf6-81a1-4c5e96e92653"), Name = "Люкс", MaxCountHumans = 2, Price = 2000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f285") },
               new ModelCategory { Id = Guid.Parse("0fbeec62-2106-4de7-9b79-89977a2d05c6"), Name = "Апартамент", MaxCountHumans = 4, Price = 3000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f285") },

               new ModelCategory { Id = Guid.Parse("a509e268-d7e3-4a53-a928-51e2dfd8df7e"), Name = "Стандарт", MaxCountHumans = 2, Price = 1000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f286") },
               new ModelCategory { Id = Guid.Parse("fa4a2888-9814-4f4e-a8c0-bed2b85275bd"), Name = "Люкс", MaxCountHumans = 2, Price = 2000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f286") },
               new ModelCategory { Id = Guid.Parse("a2073141-447e-42c0-b18d-68f47e7ebce8"), Name = "Апартамент", MaxCountHumans = 4, Price = 3000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f286") },

               new ModelCategory { Id = Guid.Parse("d6bfb4e2-abc0-468f-bf34-60a509923660"), Name = "Стандарт", MaxCountHumans = 2, Price = 1000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f288") },
               new ModelCategory { Id = Guid.Parse("466b0bf1-5b02-4bcf-8db6-faa92d86db85"), Name = "Люкс", MaxCountHumans = 2, Price = 2000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f288") },
               new ModelCategory { Id = Guid.Parse("dee51628-9693-42bd-8dbb-384dfaf2382d"), Name = "Апартамент", MaxCountHumans = 4, Price = 3000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f288") },

               new ModelCategory { Id = Guid.Parse("53a8bee1-3455-435c-b892-1d6ace10e8dd"), Name = "Стандарт", MaxCountHumans = 2, Price = 1000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f289") },
               new ModelCategory { Id = Guid.Parse("93f53406-55f9-40c5-bbd7-5491c6791b0f"), Name = "Люкс", MaxCountHumans = 2, Price = 2000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f289") },
               new ModelCategory { Id = Guid.Parse("70c56ff3-ee74-44c8-a3a8-dc1af13e7d0a"), Name = "Апартамент", MaxCountHumans = 4, Price = 3000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f289") },

               new ModelCategory { Id = Guid.Parse("e32f4aa6-89e9-4cac-b466-db75a2943ac7"), Name = "Стандарт", MaxCountHumans = 2, Price = 1000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f280") },
               new ModelCategory { Id = Guid.Parse("69d76149-2177-4526-af3b-f477467d5c12"), Name = "Люкс", MaxCountHumans = 2, Price = 2000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f280") },
               new ModelCategory { Id = Guid.Parse("a8d924be-03f5-4179-a372-a198102d8079"), Name = "Апартамент", MaxCountHumans = 4, Price = 3000, HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f280") }

            
           );

            modelBuilder.Entity<ModelGuest>().HasData(
                new ModelGuest { Id = Guid.Parse("0418bbf7-7aa5-4263-a053-8f32e07e71df"), Firstname = "Василий", Lastname = "Пупкин", Patronymic = "Петрович", Phone = "+7(915) 100-00-00"},
                new ModelGuest { Id = Guid.Parse("0418bbf7-8aa5-4263-a053-8f32e07e71df"), Firstname = "Андрей", Lastname = "Порошин", Patronymic = "Николаевич", Phone = "+7(915) 101-23-77" },
                new ModelGuest { Id = Guid.Parse("0418bbf7-9aa5-4263-a053-8f32e07e71df"), Firstname = "Анна", Lastname = "Мирпоева", Patronymic = "Алексеевна", Phone = "+7(961) 100-89-01" },
                new ModelGuest { Id = Guid.Parse("0418bbf7-1aa5-4263-a053-8f32e07e71df"), Firstname = "Мария", Lastname = "Цветаева", Patronymic = "Андреевна", Phone = "+7(915) 567-56-90" },
                new ModelGuest { Id = Guid.Parse("0418bbf7-2aa5-4263-a053-8f32e07e71df"), Firstname = "Иван", Lastname = "Иванович", Patronymic = "Иванов", Phone = "+7(909) 678-00-23" },
                new ModelGuest { Id = Guid.Parse("0418bbf7-3aa5-4263-a053-8f32e07e71df"), Firstname = "Людмила", Lastname = "Бакулевская", Patronymic = "Олеговна", Phone = "+7(987) 888-00-00" },
               new ModelGuest { Id = Guid.Parse("0418bbf7-4aa5-4263-a053-8f32e07e71df"), Firstname = "Нина", Lastname = "Сергеевна", Patronymic = "Соболева", Phone = "+7(915) 290-83-00" },
               new ModelGuest { Id = Guid.Parse("0418bbf7-5aa5-4263-a053-8f32e07e71df"), Firstname = "Максим", Lastname = "Гринев", Patronymic = "Александрович", Phone = "+7(951) 100-89-00" },
               new ModelGuest { Id = Guid.Parse("0418bbf7-6aa5-4263-a053-8f32e07e71df"), Firstname = "Данил", Lastname = "Пятнуев", Patronymic = "Григорьевич", Phone = "+7(900) 100-55-00" },
               new ModelGuest { Id = Guid.Parse("0418bbf7-0aa5-4263-a053-8f32e07e71df"), Firstname = "Оксана", Lastname = "Морозова", Patronymic = "Александровна", Phone = "+7(915) 100-33-33" }
            );

            modelBuilder.Entity<ModelClient>().HasData(
                new ModelClient { Id = Guid.Parse("e063b849-1ca8-4d5e-bec9-9041fed2b72f"), GuestId = Guid.Parse("0418bbf7-7aa5-4263-a053-8f32e07e71df"), isPhysics=true },
                new ModelClient { Id = Guid.Parse("e063b849-2ca8-4d5e-bec9-9041fed2b72f"), GuestId = Guid.Parse("0418bbf7-8aa5-4263-a053-8f32e07e71df"), isPhysics = true },
                new ModelClient { Id = Guid.Parse("e063b849-3ca8-4d5e-bec9-9041fed2b72f"), GuestId = Guid.Parse("0418bbf7-9aa5-4263-a053-8f32e07e71df"), isPhysics = true },
                new ModelClient { Id = Guid.Parse("e063b849-4ca8-4d5e-bec9-9041fed2b72f"), GuestId = Guid.Parse("0418bbf7-1aa5-4263-a053-8f32e07e71df"), isPhysics = true },
                new ModelClient { Id = Guid.Parse("e063b849-5ca8-4d5e-bec9-9041fed2b72f"), GuestId = Guid.Parse("0418bbf7-2aa5-4263-a053-8f32e07e71df"), isPhysics = true },
                new ModelClient { Id = Guid.Parse("e063b849-6ca8-4d5e-bec9-9041fed2b72f"), GuestId = Guid.Parse("0418bbf7-3aa5-4263-a053-8f32e07e71df"), isPhysics = true },
                new ModelClient { Id = Guid.Parse("e063b849-7ca8-4d5e-bec9-9041fed2b72f"), GuestId = Guid.Parse("0418bbf7-4aa5-4263-a053-8f32e07e71df"), isPhysics = true },
                new ModelClient { Id = Guid.Parse("e063b849-8ca8-4d5e-bec9-9041fed2b72f"), GuestId = Guid.Parse("0418bbf7-5aa5-4263-a053-8f32e07e71df"), isPhysics = true },
                new ModelClient { Id = Guid.Parse("e063b849-9ca8-4d5e-bec9-9041fed2b72f"), GuestId = Guid.Parse("0418bbf7-6aa5-4263-a053-8f32e07e71df"), isPhysics = true },
                new ModelClient { Id = Guid.Parse("e063b849-0ca8-4d5e-bec9-9041fed2b72f"), GuestId = Guid.Parse("0418bbf7-0aa5-4263-a053-8f32e07e71df"), isPhysics = true }
            );

            modelBuilder.Entity<ModelRoom>().HasData(
                new ModelRoom { 
                    Id = Guid.Parse("e34abd5e-6d82-403c-9dc0-bbca6a63baf4"), Number="01", 
                    HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"), CountHumans = 2,
                    CategoryId= Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91419"), 
                    StatusId=Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf13")
                },
                  new ModelRoom
                  {
                      Id = Guid.Parse("e34abd5e-6d82-403c-9dc0-bbca6a63baf5"),
                      Number = "02",
                      HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                      CountHumans = 3,
                      CategoryId = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91420"),
                      StatusId = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf14")
                  },
            new ModelRoom
            {
                Id = Guid.Parse("e34abd5e-6d82-403c-9dc0-bbca6a63baf6"),
                Number = "11",
                HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                CountHumans = 4,
                CategoryId = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91421"),
                StatusId = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf14")
            },
            new ModelRoom
            {
                Id = Guid.Parse("e34abd5e-6d82-403c-9dc0-bbca6a63baf7"),
                Number = "16",
                HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                CountHumans = 1,
                CategoryId = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91421"),
                StatusId = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf13")

            },
            new ModelRoom
            {
                Id = Guid.Parse("e34abd5e-6d82-403c-9dc0-bbca6a63baf8"),
                Number = "45",
                HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                CountHumans = 2,
                CategoryId = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91419"),
                StatusId = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf16")
            },
            new ModelRoom
            {
                Id = Guid.Parse("e34abd5e-6d82-403c-9dc0-bbca6a63baf9"),
                Number = "09",
                HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                CountHumans = 2,
                CategoryId = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91421"),
                StatusId = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf15")
            },
            new ModelRoom
            {
                Id = Guid.Parse("e24abd5e-6d82-403c-9dc0-bbca6a63baf1"),
                Number = "13",
                HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                CountHumans = 4,
                CategoryId = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91419"),
                StatusId = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf14")
            },
            new ModelRoom
            {
                Id = Guid.Parse("e54abd5e-6d82-403c-9dc0-bbca6a63baf1"),
                Number = "23",
                HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                CountHumans = 3,
                CategoryId = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91421"),
                StatusId = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf13")
            },
            new ModelRoom
            {
                Id = Guid.Parse("e74abd5e-6d82-403c-9dc0-bbca6a63baf1"),
                Number = "02",
                HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                CountHumans = 1,
                CategoryId = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91420"),
                StatusId = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf16")
            },
            new ModelRoom
            {
                Id = Guid.Parse("e84abd5e-6d82-403c-9dc0-bbca6a63baf1"),
                Number = "001",
                HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                CountHumans = 2,
                CategoryId = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91420"),
                StatusId = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf15")
            },
            new ModelRoom
            {
                Id = Guid.Parse("e94abd5e-6d82-403c-9dc0-bbca6a63baf1"),
                Number = "20",
                HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                CountHumans = 1,
                CategoryId = Guid.Parse("bc8b315c-7612-4f17-b973-19a26ee91421"),
                StatusId = Guid.Parse("98fb015f-4f59-48de-808a-c01e7a95cf14")
            }
            );
            var now = DateOnly.FromDateTime(DateTime.Now);
            modelBuilder.Entity<ModelBooking>().HasData(
                new ModelBooking()
                {
                    Id = Guid.Parse("8f339ea3-db84-4b87-a521-20d4b9a8b21c"),
                    startDate = now.AddDays(-3),
                    endDate = now,
                    Date = now.AddMonths(-1),
                    ClientId = Guid.Parse("e063b849-1ca8-4d5e-bec9-9041fed2b72f"),
                    HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                    RoomId = Guid.Parse("e34abd5e-6d82-403c-9dc0-bbca6a63baf4")

                },
                new ModelBooking()
                {
                    startDate = now,
                    Id = Guid.Parse("a8c9f4dc-99cf-4080-8bc7-153ccab2ef07"),
                   
                    endDate = now.AddDays(+5),
                    Date = now.AddMonths(-4),
                    ClientId = Guid.Parse("e063b849-2ca8-4d5e-bec9-9041fed2b72f"),
                    HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                    RoomId = Guid.Parse("e34abd5e-6d82-403c-9dc0-bbca6a63baf5")

                   
                },
                 new ModelBooking()
                 {
                     startDate = now,
                     Id = Guid.Parse("15ccdb27-f453-4dc3-a93f-ef357f07653e"),

                     endDate = now.AddDays(+1),
                     Date = now.AddMonths(-1),
                     ClientId = Guid.Parse("e063b849-3ca8-4d5e-bec9-9041fed2b72f"),
                     HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                     RoomId = Guid.Parse("e34abd5e-6d82-403c-9dc0-bbca6a63baf6")
                 },
                 new ModelBooking()
                 {
                     startDate = now,
                     Id = Guid.Parse("1421d49f-11f8-4c45-8210-42979ece7613"),

                     endDate = now.AddDays(+2),
                     Date = now.AddMonths(-1),
                     ClientId = Guid.Parse("e063b849-4ca8-4d5e-bec9-9041fed2b72f"),
                     HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                     RoomId = Guid.Parse("e34abd5e-6d82-403c-9dc0-bbca6a63baf7")
                 },
                  new ModelBooking()
                  {
                      startDate = now,
                      Id = Guid.Parse("16ae9380-92ca-4b3f-ac1c-53608ee4eed2"),

                      endDate = now.AddDays(+2),
                      Date = now.AddMonths(-3),
                      ClientId = Guid.Parse("e063b849-5ca8-4d5e-bec9-9041fed2b72f"),
                      HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                      RoomId = Guid.Parse("e34abd5e-6d82-403c-9dc0-bbca6a63baf8")
                  },
                   new ModelBooking()
                   {
                       startDate = now,
                       Id = Guid.Parse("469fd4bc-0ebb-4298-b36f-7cd163135d07"),

                       endDate = now.AddDays(+7),
                       Date = now.AddMonths(-8),
                       ClientId = Guid.Parse("e063b849-6ca8-4d5e-bec9-9041fed2b72f"),
                       HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                       RoomId = Guid.Parse("e34abd5e-6d82-403c-9dc0-bbca6a63baf9")
                   },
                    new ModelBooking()
                    {
                        Id = Guid.Parse("510a8dfc-74d2-4b5e-8d8a-a38b9869a6e2"),
                        startDate = now.AddDays(-2),
                        endDate = now,
                        Date = now.AddMonths(-1),
                        ClientId = Guid.Parse("e063b849-7ca8-4d5e-bec9-9041fed2b72f"),
                        HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                        RoomId = Guid.Parse("e24abd5e-6d82-403c-9dc0-bbca6a63baf1")

                    },
                     new ModelBooking()
                     {
                         Id = Guid.Parse("6ded2491-2434-44b9-ac56-76628405e4ef"),
                         startDate = now.AddDays(-5),
                         endDate = now,
                         Date = now.AddMonths(-2),
                         ClientId = Guid.Parse("e063b849-8ca8-4d5e-bec9-9041fed2b72f"),
                         HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                         RoomId = Guid.Parse("e54abd5e-6d82-403c-9dc0-bbca6a63baf1")

                     },
                      new ModelBooking()
                      {
                          Id = Guid.Parse("013e6167-52a3-4ef0-938b-4ddb68b0a55d"),
                          startDate = now.AddDays(-7),
                          endDate = now,
                          Date = now.AddMonths(-3),
                          ClientId = Guid.Parse("e063b849-9ca8-4d5e-bec9-9041fed2b72f"),
                          HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                          RoomId = Guid.Parse("e74abd5e-6d82-403c-9dc0-bbca6a63baf1")

                      },
                       new ModelBooking()
                       {
                           Id = Guid.Parse("e2cfbff4-65b3-4126-be3d-4d75b3bc95d1"),
                           startDate = now.AddDays(-2),
                           endDate = now,
                           Date = now.AddMonths(-6),
                           ClientId = Guid.Parse("e063b849-0ca8-4d5e-bec9-9041fed2b72f"),
                           HotelId = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"),
                           RoomId = Guid.Parse("e84abd5e-6d82-403c-9dc0-bbca6a63baf1")

                       }
            );
            modelBuilder.Entity<ModelCategoryHotel>().HasData(
                new ModelCategoryHotel() { Id = Guid.Parse("74c14e57-2bd2-4cc8-b829-97d742890dd6"), Name = "Популярное", Sale=0 },
                new ModelCategoryHotel() { Id = Guid.Parse("6470ed37-b8f0-40e9-bbf6-fc6d5f5c0ff9"), Name = "Непопулярные", Sale=10 },
                new ModelCategoryHotel() { Id = Guid.Parse("00ae514e-2f8d-4b10-a939-904e276b7452"), Name = "Средне популярное", Sale=0 }
            );
        }

        public List<ModelHotel> GetHotels() 
        { 
            return Hotels
                .Include(t => t.Region)
                .Include(t => t.CategoryHotel)
                .ToList();
        }

        public List<ModelRegion> GetRegions()
        {
            return Regions.ToList();
        }

        public List<ModelRoom> GetRoomsHotel(Guid hotelId) 
        {
            return Rooms
                .Include(t => t.Category)
                .Include(t => t.Status)
                .ToList()
                .Where(t => t.HotelId == hotelId)
                .ToList();
        }

        public List<ModelCategory> GetCategories()
        {
            return Categories
                .Include(t => t.Hotel)
                .ToList();
        }

        public List<ModelGuest> GetGuests() 
        {
            return Guests.ToList();
        }

        public List<ModelClient> GetClients() 
        {
            return Clients.Include(t => t.Guest).ToList();
        }

        public List<ModelBooking> GetBookings()
        {
            return Bookings.Include(t => t.Client).Include(t => t.Hotel).Include(t => t.Room).ToList();
        }

        public List<ModelBooking> GetBookingsForEnterAction()
        {
            return GetBookingList()
                .Where(t => t.startDate == DateOnly.FromDateTime(DateTime.Now) && !t.isCompleteEnter)
                .ToList();
        }

        public List<ModelBooking> GetBookingsForExitAction()
        {
            return GetBookingList()
                .Where(t => t.endDate == DateOnly.FromDateTime(DateTime.Now) && !t.isCompleteExit)
                .ToList();
        }

        public List<ModelBooking> GetBookingList()
        {
            return Bookings
                .Include(t => t.Room)
                .Include(t => t.Room.Category)
                .Include(t => t.Client)
                .Include(t => t.Hotel)
                .Include(t => t.Client.Guest)
                .ToList();
        }

        public int GetCountNights(DateTime start, DateTime end) 
        {
            using (Context context = new Context()) 
            {
                var bookings = GetBookingBetweenDates(start, end);
                return bookings.Sum(t => t.CountNights);
            }
        }
        public List<ModelBooking> GetBookingBetweenDates(DateTime start, DateTime end)
        {
            return Bookings
                .Include(t => t.Room)
                .Include(t => t.Room.Category)
                .Include(t => t.Client)
                .Include(t => t.Hotel)
                .Include(t => t.Client.Guest)
                .ToList()
                .Where(t => t.startDate.ToDateTime(TimeOnly.MinValue) >= start && t.endDate.ToDateTime(TimeOnly.MinValue) <= end)
                .ToList();
        }

        public int GetPercentLoading(DateTime start, DateTime end)
        { 
            var nights = GetCountNights(start, end);
            var countRooms = Rooms.ToList().Count;
            return nights / countRooms;
        }

        public float GetSumCost(DateTime start, DateTime end) 
        {
            var bookings = GetBookingBetweenDates(start, end);
            var sumCost = bookings.Sum(t => t.Cost);
            return sumCost;
        }

        public float GetADR(DateTime start, DateTime end) 
        {
            float sumCost = GetSumCost(start, end);
            int nights = GetCountNights(start, end);
            return sumCost / nights;
        }

        public float GetPevPar(DateTime start, DateTime end)
        { 
            return GetADR(start, end) * GetPercentLoading(start, end);
        }

        public ReportDataSource GetReportDataSource() 
        {
            DataTable dt = new DataTable();
            ReportDataSource rds = new ReportDataSource("DataTable1", dt);
            return rds;
        }

        public void FillParamenterReportViewer(ReportViewer reportViewer, DateTime target, string hotelName)
        {
            var count = GetCountNights(DateTime.MinValue, target).ToString();
            reportViewer.LocalReport.SetParameters(new ReportParameter("CountNights", count));
            reportViewer.LocalReport.SetParameters(new ReportParameter("ADR", GetADR(DateTime.MinValue, target).ToString()));
            reportViewer.LocalReport.SetParameters(new ReportParameter("ProcentNumberFond", GetPercentLoading(DateTime.MinValue, target).ToString()));
            reportViewer.LocalReport.SetParameters(new ReportParameter("Hotel", hotelName));
            reportViewer.LocalReport.SetParameters(new ReportParameter("SummSale", GetSumCost(DateTime.MinValue, target).ToString()));
            reportViewer.LocalReport.SetParameters(new ReportParameter("Date", target.ToString("dd.MM.yyyy")));
            reportViewer.LocalReport.SetParameters(new ReportParameter("RevPAR", GetPevPar(DateTime.MinValue, target).ToString()));
        }

    }
}
