﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NTO.database.models
{
    internal class ModelRoom
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public Guid HotelId { get; set; }

        public ModelHotel Hotel { get; set; }

        public int CountHumans { get; set; }

        public Guid CategoryId { get; set; }

        public ModelCategory Category { get; set; }

        public Guid StatusId { get; set; }

        public ModelStatus Status { get; set; }

        [NotMapped]
        public string StatusName { get { return Status.Name; } }

        [NotMapped]
        public string CategoryName { get { return Category.Name; } }
    }
}
