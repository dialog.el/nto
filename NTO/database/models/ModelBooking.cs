﻿
using System.Windows.Forms;

namespace NTO.database.models
{
    internal class ModelBooking
    {
        public Guid Id { get; set; }

        public DateOnly Date { get; set; }

        public DateOnly startDate { get; set; }

        public DateOnly endDate { get; set; }

        public Guid ClientId { get; set; }

        public Guid HotelId { get; set; }
        public Guid RoomId { get; set; }

        public ModelClient Client { get; set; }

        public ModelHotel Hotel { get; set; }
        public ModelRoom Room { get; set; }

        public string HotelName { get { return Hotel.Name; } }

        public int Cost { 
            get {
                return Room.Category.Price * CountNights;
            } 
        }

        public string Number { get { return Room.Number; } }

        public bool isCompleteEnter { get; set; } = false;
        public bool isCompleteExit { get; set; } = false;

        public int CountNights { get {
                return (endDate.ToDateTime(TimeOnly.MinValue) - startDate.ToDateTime(TimeOnly.MinValue)).Days - 1; 
            } 
        }

        public string DisplayMember { get {
                return $"{Client.FIO}, c {startDate.ToString("dd.MM.yyyy")} до {endDate.ToString("dd.MM.yyyy")}, в отеле '{Hotel.Name}', в комнате {Room.Number}";
            } 
        }

        public string FIO { get { return Client.FIO; } }
    }
}
