﻿namespace NTO.database.models
{
    public class ModelGuest
    {
        public Guid Id { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Patronymic { get; set; }

        public string Phone { get; set; }

        public string FIO { get { return $"{Lastname} {Firstname[0]}.{Patronymic[0]}";  } }
    }
}
