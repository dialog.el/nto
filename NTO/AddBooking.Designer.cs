﻿namespace NTO
{
    partial class AddBooking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddBooking));
            dateTimePickerStart = new DateTimePicker();
            label1 = new Label();
            label2 = new Label();
            dateTimePickerEnd = new DateTimePicker();
            button1 = new Button();
            comboBoxClients = new ComboBox();
            label3 = new Label();
            label4 = new Label();
            comboBoxHotels = new ComboBox();
            label5 = new Label();
            label6 = new Label();
            comboBoxRooms = new ComboBox();
            txtCost = new Label();
            txtSale = new Label();
            txtSum = new Label();
            SuspendLayout();
            // 
            // dateTimePickerStart
            // 
            dateTimePickerStart.Location = new Point(10, 28);
            dateTimePickerStart.Margin = new Padding(3, 2, 3, 2);
            dateTimePickerStart.Name = "dateTimePickerStart";
            dateTimePickerStart.Size = new Size(219, 23);
            dateTimePickerStart.TabIndex = 0;
            dateTimePickerStart.ValueChanged += dateTimePickerStart_ValueChanged;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = SystemColors.ActiveCaption;
            label1.Location = new Point(10, 11);
            label1.Name = "label1";
            label1.Size = new Size(79, 15);
            label1.TabIndex = 1;
            label1.Text = "С какой даты";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = SystemColors.ActiveCaption;
            label2.Location = new Point(234, 11);
            label2.Name = "label2";
            label2.Size = new Size(86, 15);
            label2.TabIndex = 3;
            label2.Text = "До какой даты";
            // 
            // dateTimePickerEnd
            // 
            dateTimePickerEnd.Location = new Point(234, 28);
            dateTimePickerEnd.Margin = new Padding(3, 2, 3, 2);
            dateTimePickerEnd.Name = "dateTimePickerEnd";
            dateTimePickerEnd.Size = new Size(219, 23);
            dateTimePickerEnd.TabIndex = 2;
            dateTimePickerEnd.ValueChanged += dateTimePickerEnd_ValueChanged;
            // 
            // button1
            // 
            button1.BackColor = SystemColors.ActiveCaption;
            button1.Location = new Point(10, 291);
            button1.Margin = new Padding(3, 2, 3, 2);
            button1.Name = "button1";
            button1.Size = new Size(443, 33);
            button1.TabIndex = 4;
            button1.Text = "Сохранить";
            button1.UseVisualStyleBackColor = false;
            button1.Click += button1_Click;
            // 
            // comboBoxClients
            // 
            comboBoxClients.DisplayMember = "FIO";
            comboBoxClients.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxClients.FormattingEnabled = true;
            comboBoxClients.Location = new Point(10, 92);
            comboBoxClients.Margin = new Padding(3, 2, 3, 2);
            comboBoxClients.Name = "comboBoxClients";
            comboBoxClients.Size = new Size(219, 23);
            comboBoxClients.TabIndex = 5;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.BackColor = SystemColors.ActiveCaption;
            label3.Location = new Point(10, 74);
            label3.Name = "label3";
            label3.Size = new Size(46, 15);
            label3.TabIndex = 6;
            label3.Text = "Клиент";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(234, 74);
            label4.Name = "label4";
            label4.Size = new Size(0, 15);
            label4.TabIndex = 8;
            // 
            // comboBoxHotels
            // 
            comboBoxHotels.DisplayMember = "NameWithCategoryHotel";
            comboBoxHotels.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxHotels.FormattingEnabled = true;
            comboBoxHotels.Location = new Point(10, 141);
            comboBoxHotels.Margin = new Padding(3, 2, 3, 2);
            comboBoxHotels.Name = "comboBoxHotels";
            comboBoxHotels.Size = new Size(444, 23);
            comboBoxHotels.TabIndex = 7;
            comboBoxHotels.SelectedIndexChanged += comboBoxHotels_SelectedIndexChanged;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.BackColor = SystemColors.ActiveCaption;
            label5.Location = new Point(10, 123);
            label5.Name = "label5";
            label5.Size = new Size(40, 15);
            label5.TabIndex = 9;
            label5.Text = "Отель";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.BackColor = SystemColors.ActiveCaption;
            label6.Location = new Point(235, 74);
            label6.Name = "label6";
            label6.Size = new Size(54, 15);
            label6.TabIndex = 11;
            label6.Text = "Комната";
            // 
            // comboBoxRooms
            // 
            comboBoxRooms.DisplayMember = "Number";
            comboBoxRooms.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBoxRooms.FormattingEnabled = true;
            comboBoxRooms.Location = new Point(235, 92);
            comboBoxRooms.Margin = new Padding(3, 2, 3, 2);
            comboBoxRooms.Name = "comboBoxRooms";
            comboBoxRooms.Size = new Size(219, 23);
            comboBoxRooms.TabIndex = 10;
            comboBoxRooms.SelectedIndexChanged += comboBoxRooms_SelectedIndexChanged;
            // 
            // txtCost
            // 
            txtCost.AutoSize = true;
            txtCost.BackColor = SystemColors.ControlLight;
            txtCost.Font = new Font("Segoe UI", 14F, FontStyle.Regular, GraphicsUnit.Point);
            txtCost.Location = new Point(10, 175);
            txtCost.Name = "txtCost";
            txtCost.Size = new Size(109, 25);
            txtCost.TabIndex = 12;
            txtCost.Text = "Стоимость:";
            // 
            // txtSale
            // 
            txtSale.AutoSize = true;
            txtSale.BackColor = SystemColors.ControlLight;
            txtSale.Font = new Font("Segoe UI", 14F, FontStyle.Regular, GraphicsUnit.Point);
            txtSale.Location = new Point(10, 212);
            txtSale.Name = "txtSale";
            txtSale.Size = new Size(82, 25);
            txtSale.TabIndex = 13;
            txtSale.Text = "Скидка: ";
            // 
            // txtSum
            // 
            txtSum.AutoSize = true;
            txtSum.BackColor = SystemColors.ControlLight;
            txtSum.Font = new Font("Segoe UI", 14F, FontStyle.Regular, GraphicsUnit.Point);
            txtSum.Location = new Point(12, 245);
            txtSum.Name = "txtSum";
            txtSum.Size = new Size(67, 25);
            txtSum.TabIndex = 14;
            txtSum.Text = "Итого:";
            // 
            // AddBooking
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            ClientSize = new Size(463, 335);
            Controls.Add(txtSum);
            Controls.Add(txtSale);
            Controls.Add(txtCost);
            Controls.Add(label6);
            Controls.Add(comboBoxRooms);
            Controls.Add(label5);
            Controls.Add(label4);
            Controls.Add(comboBoxHotels);
            Controls.Add(label3);
            Controls.Add(comboBoxClients);
            Controls.Add(button1);
            Controls.Add(label2);
            Controls.Add(dateTimePickerEnd);
            Controls.Add(label1);
            Controls.Add(dateTimePickerStart);
            Margin = new Padding(3, 2, 3, 2);
            Name = "AddBooking";
            Text = "Добавление бронирования";
            Load += AddBooking_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private DateTimePicker dateTimePickerStart;
        private Label label1;
        private Label label2;
        private DateTimePicker dateTimePickerEnd;
        private Button button1;
        private ComboBox comboBoxClients;
        private Label label3;
        private Label label4;
        private ComboBox comboBoxHotels;
        private Label label5;
        private Label label6;
        private ComboBox comboBoxRooms;
        private Label txtCost;
        private Label txtSale;
        private Label txtSum;
    }
}