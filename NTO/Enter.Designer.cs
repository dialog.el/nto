﻿namespace NTO
{
    partial class Enter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            cbEnter = new ComboBox();
            label1 = new Label();
            btnEnter = new Button();
            btnEnter2 = new Button();
            label2 = new Label();
            SuspendLayout();
            // 
            // cbEnter
            // 
            cbEnter.DisplayMember = "DisplayMember";
            cbEnter.DropDownStyle = ComboBoxStyle.DropDownList;
            cbEnter.FormattingEnabled = true;
            cbEnter.Location = new Point(12, 96);
            cbEnter.Name = "cbEnter";
            cbEnter.Size = new Size(573, 23);
            cbEnter.TabIndex = 0;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = SystemColors.ControlLight;
            label1.Location = new Point(12, 66);
            label1.Name = "label1";
            label1.Size = new Size(229, 15);
            label1.TabIndex = 1;
            label1.Text = "Список броней на оформление сегодня";
            // 
            // btnEnter
            // 
            btnEnter.BackColor = SystemColors.ActiveCaption;
            btnEnter.Location = new Point(12, 134);
            btnEnter.Name = "btnEnter";
            btnEnter.Size = new Size(147, 39);
            btnEnter.TabIndex = 2;
            btnEnter.Text = "Оформить";
            btnEnter.UseVisualStyleBackColor = false;
            btnEnter.Click += btnEnter_Click;
            // 
            // btnEnter2
            // 
            btnEnter2.BackColor = SystemColors.ActiveCaption;
            btnEnter2.Location = new Point(438, 134);
            btnEnter2.Name = "btnEnter2";
            btnEnter2.Size = new Size(147, 45);
            btnEnter2.TabIndex = 3;
            btnEnter2.Text = "Заезд без брони";
            btnEnter2.UseVisualStyleBackColor = false;
            btnEnter2.Click += btnEnter2_Click;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(12, 9);
            label2.Name = "label2";
            label2.Size = new Size(184, 32);
            label2.TabIndex = 4;
            label2.Text = "К оформлению";
            // 
            // Enter
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(597, 192);
            Controls.Add(label2);
            Controls.Add(btnEnter2);
            Controls.Add(btnEnter);
            Controls.Add(label1);
            Controls.Add(cbEnter);
            Name = "Enter";
            Text = "Заезд";
            Load += Enter_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ComboBox cbEnter;
        private Label label1;
        private Button btnEnter;
        private Button btnEnter2;
        private Label label2;
    }
}