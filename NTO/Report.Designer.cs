﻿namespace NTO
{
    partial class Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            panel1 = new Panel();
            reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            btnReport = new Button();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.Controls.Add(reportViewer1);
            panel1.Dock = DockStyle.Bottom;
            panel1.Location = new Point(0, 66);
            panel1.Name = "panel1";
            panel1.Size = new Size(800, 384);
            panel1.TabIndex = 0;
            panel1.Paint += panel1_Paint;
            // 
            // reportViewer1
            // 
            reportViewer1.Dock = DockStyle.Fill;
            reportViewer1.Location = new Point(0, 0);
            reportViewer1.Name = "ReportViewer";
            reportViewer1.ServerReport.BearerToken = null;
            reportViewer1.Size = new Size(800, 384);
            reportViewer1.TabIndex = 0;
            // 
            // btnReport
            // 
            btnReport.Location = new Point(549, 18);
            btnReport.Name = "btnReport";
            btnReport.Size = new Size(199, 33);
            btnReport.TabIndex = 1;
            btnReport.Text = "Создать отчёт";
            btnReport.UseVisualStyleBackColor = true;
            btnReport.Click += btnReport_Click;
            // 
            // Report
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(btnReport);
            Controls.Add(panel1);
            Name = "Report";
            Text = "Отчет о стоимости категорий";
            panel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private Panel panel1;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private Button btnReport;
    }
}