﻿namespace NTO
{
    partial class EditCategories
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditCategories));
            btnEditCategories = new Button();
            comboBoxCategory = new ComboBox();
            comboBoxHotel = new ComboBox();
            numericUpDownCost = new NumericUpDown();
            label1 = new Label();
            label2 = new Label();
            ((System.ComponentModel.ISupportInitialize)numericUpDownCost).BeginInit();
            SuspendLayout();
            // 
            // btnEditCategories
            // 
            btnEditCategories.BackColor = SystemColors.ActiveCaption;
            btnEditCategories.Location = new Point(136, 170);
            btnEditCategories.Name = "btnEditCategories";
            btnEditCategories.Size = new Size(140, 39);
            btnEditCategories.TabIndex = 1;
            btnEditCategories.Text = "Сохранить изменения";
            btnEditCategories.UseVisualStyleBackColor = false;
            btnEditCategories.Click += btnEditCategories_Click;
            // 
            // comboBoxCategory
            // 
            comboBoxCategory.DisplayMember = "Name";
            comboBoxCategory.FormattingEnabled = true;
            comboBoxCategory.Location = new Point(12, 26);
            comboBoxCategory.Name = "comboBoxCategory";
            comboBoxCategory.Size = new Size(264, 23);
            comboBoxCategory.TabIndex = 2;
            comboBoxCategory.TextChanged += comboBoxCategory_TextChanged;
            // 
            // comboBoxHotel
            // 
            comboBoxHotel.DisplayMember = "Name";
            comboBoxHotel.FormattingEnabled = true;
            comboBoxHotel.Location = new Point(12, 76);
            comboBoxHotel.Name = "comboBoxHotel";
            comboBoxHotel.Size = new Size(264, 23);
            comboBoxHotel.TabIndex = 3;
            comboBoxHotel.SelectedIndexChanged += comboBoxHotel_SelectedIndexChanged;
            // 
            // numericUpDownCost
            // 
            numericUpDownCost.Location = new Point(12, 118);
            numericUpDownCost.Maximum = new decimal(new int[] { 1410065407, 2, 0, 0 });
            numericUpDownCost.Name = "numericUpDownCost";
            numericUpDownCost.Size = new Size(264, 23);
            numericUpDownCost.TabIndex = 4;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = SystemColors.ControlLight;
            label1.Location = new Point(12, 8);
            label1.Name = "label1";
            label1.Size = new Size(63, 15);
            label1.TabIndex = 5;
            label1.Text = "Категория";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = SystemColors.ControlLight;
            label2.Location = new Point(15, 58);
            label2.Name = "label2";
            label2.Size = new Size(40, 15);
            label2.TabIndex = 6;
            label2.Text = "Отель";
            // 
            // EditCategories
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.WhiteSmoke;
            BackgroundImage = (Image)resources.GetObject("$this.BackgroundImage");
            BackgroundImageLayout = ImageLayout.Center;
            ClientSize = new Size(288, 221);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(numericUpDownCost);
            Controls.Add(comboBoxHotel);
            Controls.Add(comboBoxCategory);
            Controls.Add(btnEditCategories);
            Name = "EditCategories";
            Text = "Изменение категорий";
            Load += EditCategories_Load;
            ((System.ComponentModel.ISupportInitialize)numericUpDownCost).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnEditCategories;
        private ComboBox comboBoxCategory;
        private ComboBox comboBoxHotel;
        private NumericUpDown numericUpDownCost;
        private Label label1;
        private Label label2;
    }
}