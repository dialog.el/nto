﻿using NTO.database;
using NTO.database.models;

namespace NTO
{
    public partial class Guests : Form
    {
        List<ModelGuest> guests;
        public Guests()
        {
            InitializeComponent();
        }

        private void Guests_Load(object sender, EventArgs e)
        {
            dgvGuests.AutoGenerateColumns = false;
            using (Context context = new())
            {
                guests = context.GetGuests();
                dgvGuests.DataSource = guests;
            }
        }

        private void btnGuests_Click(object sender, EventArgs e)
        {
            var form = new AddGuests();
            form.Closed += Closed;
            form.Show();
        }

        public void Closed(object sender, EventArgs e)
        {
            using (Context context = new())
            {
                guests = context.GetGuests();
                dgvGuests.DataSource = guests;
            }
        }
    }
}
